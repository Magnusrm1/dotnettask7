﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    class Dove : Bird
    {

        //Inherits all properties of Bird and Animal
        public Dove(string name) : base(name) { }
        public Dove(string name, string habitat, int wingspan) : base(name, habitat, wingspan) { }

        public override void LayEggs()
        {
            int amount = 2;
            Console.WriteLine($"The dove {Name} laid {amount} eggs!");
        }
    }
}
