﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    class Horse : Animal
    {
        //Inherits all properties of Animal
        public int HurdlesJumped { get; set; }

        public Horse(string name) : base(name)
        {
            HurdlesJumped = 0;
        }

        public Horse(string name, int hurdlesJumped) : base(name)
        {
            HurdlesJumped = hurdlesJumped;
        }

        public void JumpHurdle()
        {
            HurdlesJumped++;
            Console.WriteLine(Name + " jumped a hurdle!");
        }
    }
}
