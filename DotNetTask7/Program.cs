﻿using System;
using System.Collections.Generic;

namespace DotNetTask7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Horse> stable = new List<Horse>();
            stable.Add(new Horse("Hank"));
            stable.Add(new Horse("Henry"));
            stable.Add(new Horse("Helen", 6));

            
            stable[2].JumpHurdle();
            stable[1].JumpHurdle();
            stable[2].JumpHurdle();

            Console.WriteLine($"{stable[2].Name} has jumped {stable[2].HurdlesJumped} hurdles.");

            // Bird list can hold both Ostrich and Dove.
            List<Bird> aviary = new List<Bird>();
            aviary.Add(new Ostrich("Hilda", "Savannah", 200));
            aviary.Add(new Dove("Karla", "Dove-place", 72));

            // Birds have common behaviors that are utilized here
            foreach (Bird bird in aviary)
            {
                Console.WriteLine($"The bird {bird.Name} has a {bird.Wingspan}cm long wingspan\n" +
                                  $"and prefers the {bird.Habitat} habitat.");
                bird.LayEggs();
                bird.Airstrike();
            }

        }
    }
}
